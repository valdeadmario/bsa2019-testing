import CartParser from "./CartParser";
import { readFileSync } from "fs";
import * as uuid from "uuid";

let parser, parse, validate, parseLine, readFile;

jest.mock("uuid");

beforeEach(() => {
  parser = new CartParser();
  parse = parser.parse.bind(parser);
  validate = parser.validate.bind(parser);
  parseLine = parser.parseLine.bind(parser);
  readFile = parser.readFile.bind(parser);
});

describe("CartParser - unit tests", () => {
  describe("parse", () => {
    // maybe its integration test
    it("should return object with 'items' and 'total' when element of file of path is require", () => {
      parser.readFile = jest.fn(() => {
        return `Product name,Price,Quantity
        Mollis consequat,10.00,2`;
      });

      expect(parse("path")).toEqual({
        items: [
          { id: undefined, name: "Mollis consequat", price: 10, quantity: 2 }
        ],
        total: 20
      });
    });

    it("should called one time console.error and throw error when element of file of path is not require", () => {
      parser.readFile = jest.fn(() => {
        return `Product name`;
      });

      console.error = jest.fn();

      expect(() => {
        parse("path");
      }).toThrow();
      expect(console.error).toHaveBeenCalledTimes(1);
    });
  });

  describe("validate", () => {
    it("should return array of error with 3 elements when contents is no-validate string", () => {
      expect(validate("hello")).toEqual([
        {
          column: 0,
          message:
            'Expected header to be named "Product name" but received hello.',
          row: 0,
          type: "header"
        },
        {
          column: 1,
          message:
            'Expected header to be named "Price" but received undefined.',
          row: 0,
          type: "header"
        },
        {
          column: 2,
          message:
            'Expected header to be named "Quantity" but received undefined.',
          row: 0,
          type: "header"
        }
      ]);
    });

    it("should return empty array when contents is validate string", () => {
      expect(
        validate(`Product name,Price,Quantity
	Mollis consequat,9.00,2`)
      ).toEqual([]);
    });

    it("should return empty array when contents is only headers", () => {
      expect(validate(`Product name,Price,Quantity`)).toEqual([]);
    });

    it("should return array of error with 3 elements when contents is only body", () => {
      expect(validate(`Mollis consequat,9.00,2`)).toEqual([
        {
          column: 0,
          message:
            'Expected header to be named "Product name" but received Mollis consequat.',
          row: 0,
          type: "header"
        },
        {
          column: 1,
          message: 'Expected header to be named "Price" but received 9.00.',
          row: 0,
          type: "header"
        },
        {
          column: 2,
          message: 'Expected header to be named "Quantity" but received 2.',
          row: 0,
          type: "header"
        }
      ]);
    });

    it("should return array of error with 1 element when contents without third params in a headers", () => {
      expect(
        validate(`Product name,Price
	  Mollis consequat,9.00,2`)
      ).toEqual([
        {
          column: 2,
          message:
            'Expected header to be named "Quantity" but received undefined.',
          row: 0,
          type: "header"
        }
      ]);
    });

    it("should return array of error with 1 element when contents without one cells in a body", () => {
      expect(
        validate(`Product name,Price,Quantity
	Mollis consequat,9.00`)
      ).toEqual([
        {
          column: -1,
          message: "Expected row to have 3 cells but received 2.",
          row: 1,
          type: "row"
        }
      ]);
    });

    it("should return array of error with 1 element when first cells of body contents is empty string", () => {
      expect(
        validate(`Product name,Price,Quantity
	,9.00,2`)
      ).toEqual([
        {
          column: 0,
          message: 'Expected cell to be a nonempty string but received "".',
          row: 1,
          type: "cell"
        }
      ]);
    });

    it("should return array of error with 1 element when price cells of body contents is not number", () => {
      expect(
        validate(`Product name,Price,Quantity
	Mollis consequat, hellow ,2`)
      ).toEqual([
        {
          column: 1,
          message:
            'Expected cell to be a positive number but received "hellow".',
          row: 1,
          type: "cell"
        }
      ]);
    });
  });

  describe("parseLine", () => {
    it("should return array with id, name, price, quantity when csvLine is a string of the format 'name,price,quantity'", () => {
      expect(parseLine(`Mollis consequat, 9.80 ,2`)).toEqual({
        id: undefined,
        name: "Mollis consequat",
        price: 9.8,
        quantity: 2
      });
    });
  });
});

describe("CartParser - integration test", () => {
  // it("", () => {
  //   expect(readFile("../samples/cart.csv")).toEqual([]);
  // });
});
